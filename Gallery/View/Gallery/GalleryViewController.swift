//
//  GalleryViewController.swift
//  Gallery
//
//  Created by Toni García Alhambra on 07/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit
import Vision

protocol GalleryView {
    func presentPicker()
    func deleteImage(image: UIImage)
}

class GalleryViewController: UIViewController, UINavigationControllerDelegate {
    
    // Views
    @IBOutlet weak var galleryCollectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    // Button outlets
    @IBOutlet weak var siguiendoIndicator: UIView!
    @IBOutlet weak var notificacionesIndicator: UIView!
    
    let imagePicker = UIImagePickerController()
    
    private var photos: [UIImage] = []
    
    var followSelected: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // galleryCollectionView
        galleryCollectionView.delegate = self
        galleryCollectionView.dataSource = self
        galleryCollectionView.register(UINib(nibName: "AddPhotoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "addPhotoCell")
        galleryCollectionView.register(UINib(nibName: "PhotoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "photoCell")
        
        // imagePicker
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = false
        
        // tableView
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "Post", bundle: nil), forCellReuseIdentifier: "post")
        tableView.register(UINib(nibName: "Notification", bundle: nil), forCellReuseIdentifier: "notification")
    }
    
    @IBAction func siguiendoButtonPressed(_ sender: UIButton) {
        siguiendoIndicator.backgroundColor = UIColor.orange
        notificacionesIndicator.backgroundColor = UIColor.white
        
        followSelected = true
        
        tableView.reloadData()
    }
    
    @IBAction func notificacionesButtonPressed(_ sender: UIButton) {
        siguiendoIndicator.backgroundColor = UIColor.white
        notificacionesIndicator.backgroundColor = UIColor.orange
        
        followSelected = false
        
        tableView.reloadData()
    }
    
}

extension GalleryViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row < photos.count {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath) as! PhotoCollectionViewCell
           
            cell.galleryView = self
            let image: UIImage = photos[indexPath.row]
            cell.image.image = image
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "addPhotoCell", for: indexPath) as! AddPhotoCollectionViewCell
            cell.galleryView = self
            
            return cell
        }
    }
}

extension GalleryViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let userPickedimage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            fatalError("ERROR_Could not convert image to UIImage.")
        }
        
        photos.append(userPickedimage)
        
        galleryCollectionView.reloadData()
        
        imagePicker.dismiss(animated: true, completion: nil)
    }
}

extension GalleryViewController: GalleryView {
    func deleteImage(image: UIImage) {
        photos.remove(at: photos.firstIndex(of: image)!)
        galleryCollectionView.reloadData()
    }
    
    func presentPicker() {
         present(imagePicker, animated: true, completion: nil)
    }
}

extension GalleryViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = followSelected ? tableView.dequeueReusableCell(withIdentifier: "post", for: indexPath) : tableView.dequeueReusableCell(withIdentifier: "notification", for: indexPath)
        
        return cell
    }
    
}
