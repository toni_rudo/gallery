//
//  PhotoCollectionViewCell.swift
//  Gallery
//
//  Created by Toni García Alhambra on 07/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit
import Lottie

class PhotoCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var deleteView: UIView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var deleteButton: UIButton!
    
    let animationView = LOTAnimationView(name: "delete")
    
    var galleryView: GalleryView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        // "Delete" animation
        animationView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        deleteView.addSubview(animationView)
        deleteView.addSubview(deleteButton)
        
        image.clipsToBounds = true
        image.layer.cornerRadius = 10
    }
    
    @IBAction func deleteButtonPressed(_ sender: UIButton) {
        print("Delete button pressed")
        
        animationView.play(fromFrame: 0, toFrame: 80) { (finished) in
            print("deleted")
            self.galleryView.deleteImage(image: self.image.image!)
            self.animationView.setProgressWithFrame(0)
        }
    }
}
