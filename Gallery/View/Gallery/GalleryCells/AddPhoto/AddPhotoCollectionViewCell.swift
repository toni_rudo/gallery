//
//  AddPhotoCollectionViewCell.swift
//  Gallery
//
//  Created by Toni García Alhambra on 07/03/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit
import Lottie

class AddPhotoCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var addView: UIView!
    @IBOutlet weak var addButton: UIButton!
    
    var galleryView: GalleryView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // "Add" animation
        let animationView = LOTAnimationView(name: "add")
        
        animationView.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        
        addView.addSubview(animationView)
        addView.addSubview(addButton)
        animationView.play()
    }
    
    @IBAction func addPhotoButton(_ sender: UIButton) {
        print("add button pressed")
        galleryView.presentPicker()
    }
}
